import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

np.random.seed(1)
tf.compat.v1.set_random_seed(1)

X = tf.compat.v1.placeholder('float')
Y = tf.compat.v1.placeholder('float')

x = np.array([3.3,4.5,6.4,7,7.5,7.9,8.3,8.6,8.9,9.4,9.6])
y = np.array([47.9,52.7,59.3,61.7,63.7,65.4,66.9,68.3,69.6,72.1,73.3])

n = len(x)

plt.scatter(x,y)
plt.xlabel('can nang')
plt.ylabel('chieu cao')

W = tf.Variable(np.random.rand(),'W')
b = tf.Variable(np.random.rand(),'b')

learning_rate = 0.001
training_epochs =110000

y_pred = tf.add(tf.multiply(X,W),b)
cost = tf.reduce_sum(tf.pow(y_pred-Y,2))/(2*n)

op = tf.compat.v1.train.GradientDescentOptimizer(learning_rate).minimize(cost)
init = tf.compat.v1.global_variables_initializer()

with tf.compat.v1.Session() as sess:
    sess.run(init)
    for epochs in range(training_epochs):
        for (_x,_y) in zip(x,y):
            sess.run(op,feed_dict={X:_x,Y:_y})
            if(epochs+1)%50 == 0:
                c = sess.run(cost,feed_dict={X:x,Y:y})
                print("epochs: ",epochs+1,", cost = ",c, ",  W = ",sess.run(W),", b = ",sess.run(b))

    training_cost = sess.run(cost,feed_dict={X:x,Y:y})
    weight = sess.run(W)
    bias = sess.run(b)

predic = weight*x + bias
print("Training cost =", training_cost, "Weight =", weight, "bias =", bias, '\n')

plt.plot(x, y, 'ro', label ='Original data') 
plt.plot(x, predic, label ='Fitted line') 
plt.title('Linear Regression Result') 
plt.legend() 
plt.show() 


plt.show()