
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression


X = np.array([[3.3,4.5,6.4,7,7.5,7.9,8.3,8.6,8.9,9.4,9.6]]).T

y = np.array([47.9,52.7,59.3,61.7,63.7,65.4,66.9,68.3,69.6,72.1,73.3])

plt.plot(X,y,'bo')
plt.xlabel('Cân nặng (kg)')
plt.ylabel('Chiều cao (cm)')

lr = LinearRegression()
model = lr.fit(X,y)
y_test = model.predict([[5.6],[9.2],[7]])

print('x test 1: 5.6, y test 1: ',y_test[0])
print('x test 1: 7, y test 3: ',y_test[2])
print('x test 1: 9.2, y test 2: ',y_test[1])

ls = np.linspace(3,10,2)
y0 = ls*model.coef_[0] + model.intercept_

print("w1 = ",model.coef_[0],": w0 = ",model.intercept_)
plt.plot(ls,y0)
plt.axis([3,10,46,74])
plt.show()