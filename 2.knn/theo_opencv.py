import numpy as np
import matplotlib.pyplot as plt
import cv2
from sklearn.metrics import accuracy_score

img = cv2.imread('digits.png',0)

cells = [np.hsplit(row,100) for row in np.vsplit(img,50)]

cells = np.array(cells)

train_data = cells[:,:90].reshape(-1,400).astype(np.float32)
test_data = cells[:,90:100].reshape(-1,400).astype(np.float32)

k = np.arange(10)
train_labels = np.repeat(k,450)
test_label = np.repeat(k,50)
knn = cv2.ml.KNearest_create()
knn.train(train_data,cv2.ml.ROW_SAMPLE,train_labels)

# my_img = cv2.imread('so4.png',0)
# data = my_img.reshape(-1,400).astype(np.float32)

kq1,kq2,kq3,kq4 = knn.findNearest(test_data,3)


#print(kq1) # return
#print(kq2) # ket qua
#print(kq3) # cac so gan nhat
#print(kq4) # khoang cach den cac so gan nhat
print('{}%'.format(100*accuracy_score(kq2,test_label)))